﻿using System;

namespace testeAc
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputToQuestionOne = "7ABC6";            
            int returnQuestionOne = QuestionOne(inputToQuestionOne);

            int[] inputToQuestionTwon = { -10, 100, -9 };
            int[] returnQuestionTwo = QuestionTwo(inputToQuestionTwon);
        }

        public static int[] QuestionTwo(int[] vs)
        {
           // int[] list = { -2, 1, -10 };
            int[] result = new int[2];

            int aux1 = vs[0];
            int aux2 = vs[1];

            for (int i = 1; i < vs.Length; i++)
            {
                if (vs[i] > aux1)
                {
                    aux2 = aux1;
                    aux1 = vs[i];
                }
                else if (vs[i] > aux2)
                    aux2 = vs[i];
            }
            result[0] = aux1;
            result[1] = aux2;
            return result;
        }
        public static int QuestionOne(string numberHexadecimal = "1F")
        {
            int stringLength = numberHexadecimal.Length;
            int hexadecimalBase = 16;
            int auxPotBase = 1;
            int result = 0;

            for (int i = stringLength - 1; i >= 0; i--)
            {
                var item = numberHexadecimal[i];
                int number = _getNumber(item);
                result += (number * auxPotBase);
                auxPotBase *= hexadecimalBase;
            }
            return result;

        }

        private static int _getNumber(char item)
        {
            int hexadecimalBase = 16;
            int codeChar_A_hexadecimal = 65;
            var codeChar = (int)item;

            int resultCodeChar = codeChar % hexadecimalBase;
            if (codeChar >= codeChar_A_hexadecimal)
                resultCodeChar += 9;

            return resultCodeChar;

        }
    }
}
